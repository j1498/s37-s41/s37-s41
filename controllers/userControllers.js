const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// User Registration
module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		userName: reqBody.userName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then(( user, error) => {

		if(error){
			return false
		} else {
			return "Successfully Registered."
		}
	})
}


// Controller to set User as an Admin

module.exports.setAsAdmin = async (data) => {

	// Verify User
	if (data.userRole) {
		return User.findById(data.userId).then((user, error) => {

			if(error) {
				return false

			} else {
				user.isAdmin = true;

				return user.save().then(result => result)
			}
		});

	} else {
		return "Only Admins can set this role."
	}
}


// User Authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if (result == null) {
			return false
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return { access: auth.createAccessToken(result)}

			} else {
				return "Please check if your email or password is correct."
			}
		}
	});
}