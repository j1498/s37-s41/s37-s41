const Order = require("../models/Order");


// Controller to create new Order

module.exports.createOrder = (data) => {
	let newOrder = new Order({
		userId: data.userId,
		products: [
			{
				productId: data.productId,
				quantity: data.quantity,
				price: data.price
			},
		],
		totalAmount: data.quantity * data.price
	});

	if (data.isAdmin) {
		return false

	} else {
	return newOrder.save().then((order, error) => {

		if (error) {
			return false

		} else {
			return order
		} 
	});
}
};



//Controller for Retrieving ALL Orders

module.exports.getAllOrders = (data) => {

	return Order.find({}).then((order) => {

		if (data.isAdmin) {
			return order

		} else {
			return "For Admin users only."
		} 
	})
};


// Controller for Retrieving User's Orders

module.exports.getUserOrders = (data) => {

	return Order.findOne({ userId: data.userId }).then((order) => {

		if (data.isAdmin) {
			return false

		} else {
			return order
		}
	});
};






/* Create Order Sample Workflow
    1. An authenticated NON-admin user sends a POST request containing a JWT in its header to the /users/checkout endpoint.
    2. API validates user identity via JWT, returns false if validation fails.
    3. If validation successful, API creates order using the contents of the request body.

    Retrieve All Orders Sample Workflow
    1. A GET request containing a JWT in its header is sent to the /users/orders endpoint.
    2. API validates user is an admin via JWT, returns false if validation fails.
    3. If validation is successful, API retrieves all orders and returns them in its response.
    
  Retrieve Authenticated User’s Orders Sample Workflow
    1. A GET request containing a JWT in its header is sent to the /users/myOrders endpoint.
    2. API validates user is NOT an admin via JWT, returns false if validation fails.
    3. If validation is successful, API retrieves orders belonging to authenticated user and returns them in its response.
    */