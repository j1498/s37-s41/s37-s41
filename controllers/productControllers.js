const Product = require("../models/Product");


/*
Create Product Sample Workflow
  1. An authenticated admin user sends a POST request containing a JWT in its header to the /products endpoint.
  2. API validates JWT, returns false if validation fails.
  3. If validation successful, API creates product using the contents of the request body.
*/


// Controller for Adding a Product

module.exports.addProduct = (reqBody) => {

	let newProduct = new Product ({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price

	});

	return newProduct.save().then((product, error) => {

		if(error) {
			return false
		} else {
			return "One Product Added"
		}
	});
}




// Controller for Retrieving ALL active Products

module.exports.getAllActive = () => {

	return Product.find({isActive: true}).then(
		result => {
			return result
		})
}



// Controller for Retrieving a Single Product

module.exports.getProduct = (reqParams) => {
 
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}


// Controller for Updating a Product

module.exports.updateProduct = (data) => {
	console.log(data)

return Product.findById(data.productId).then((result, error) => {

	console.log(result);

	if(data.isAdmin) {

		result.name = data.updateProduct.name
		result.description = data.updateProduct.description
		result.price = data.updateProduct.price

		console.log(result)

		return result.save().then((updatedProduct, error) => {

			if(error) {
				return false
			} else {
				return updatedProduct
			}

		})

	} else {
		return "You are not an Admin to perform this action."
	}

})

}


// Controller to Archive a Product

module.exports.archiveProduct = (data) => {

	return Product.findById(data.productId).then((result, error) => {

		if(data.payload === true) {
			result.isActive = false;

			return result.save().then((archivedProduct, error) => {

				if(error) {

					return false;

				} else {

					return true;
				}
			})

		} else {

			return false
		}

	})
}