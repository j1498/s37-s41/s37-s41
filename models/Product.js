const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({

  name: {
    type: String,
    required: [true, "Product name is required"]
  },
  description: {
    type: String,
    required: [true, "Description is required"]
  },
  price: {
    type: Number,
    required: [true, "Price is required"]
  },
  isActive: {
    type: Boolean,
    default: true
  },
  createdOn: {
    type: Date,
    default: new Date()
  }

});


module.exports = mongoose.model("Product", productSchema)



/* 
Data Model Requirements
  - User
    - Email (String)
    - Password (string)
    - isAdmin (Boolean - defaults to false)

  - Product
    - Name (String)
    - Description (String)
    - Price (Number)
    - isActive (Boolean - defaults to true)
    - createdOn (Date - defaults to current date of creation)

 - Order
    - totalAmount (Number)
    - purchasedOn (Date - defaults to current date of creation)
    -  Must be associated with:
      --  A user who owns the order
      --  Products that belong to the order
*/
 