const mongoose = require("mongoose");

// User model
const userSchema = new mongoose.Schema({
   firstName: {
      type: String,
      required: [true, "Please enter your first name!"]
    },
    lastName: {
      type: String,
      required: [true, "Please enter your last name!"]
    },
    userName: {
      type: String,
      required: [true, "Please enter your user name!"]
    },
    email: {
      type: String,
      required: [true, "Please enter your email!"],
      unique: true
    },
    mobileNo: {
      type: Number,
      required: [true, "Please enter your mobile number!"]
    },
    password: {
      type: String,
      required: [true, "Password is required."]
    },
    isAdmin: {
      type: Boolean,
      default: false,
    },
  
});


module.exports = mongoose.model("User", userSchema);



/*User Credentials:
  - Admin User:
    -- email: admin@email.com
    -- password: admin123
  - Dummy Customer
    -- email: customer@mail.com
    -- password: customer123

  
Data Model Requirements
  - User
    - Email (String)
    - Password (string)
    - isAdmin (Boolean - defaults to false)

  - Product
    - Name (String)
    - Description (String)
    - Price (Number)
    - isActive (Boolean - defaults to true)
    - createdOn (Date - defaults to current date of creation)


 - Order
    - totalAmount (Number)
    - purchasedOn (Date - defaults to current date of creation)
    -  Must be associated with:
      --  A user who owns the order
      --  Products that belong to the order
*/
 

 // Order

  /*Create Order Sample Workflow
    1. An authenticated NON-admin user sends a POST request containing a JWT in its header to the /users/checkout endpoint.
    2. API validates user identity via JWT, returns false if validation fails.
    3. If validation successful, API creates order using the contents of the request body.

*/