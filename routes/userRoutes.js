const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth");


// Route for Checking Email

router.post("/checkEmail", (req, res) => {
    userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})


// Route for User Registration

router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send (resultFromController))
});


// Route for User Authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});


// Route to set User as an Admin

router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {

    let data = {
        userRole: auth.decode(req.headers.authorization).isAdmin,
        userId: req.params.userId
    }
     userController.setAsAdmin(data).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving user details

router.get("/details", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);
    console.log(userData);

    userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
});



module.exports = router;




/*
Set User as Admin Sample Workflow
 1. An authenticated admin user sends a PUT request containing a JWT in its header to the /:userId/setAsAdmin endpoint.
 2. API validates JWT, returns false if validation fails.
 3. If validation successful, API finds user with ID matching the userId URL parameter and sets its isAdmin property to true.*/


