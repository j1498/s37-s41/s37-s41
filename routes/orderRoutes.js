const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderControllers");
const auth = require("../auth");


// Route to create new Order

router.post("/checkout", auth.verify, (req, res) => {

	const data = {

		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity,
		price: req.body.price,
		totalAmount: req.body,
	};

	orderController
		.createOrder(data)
		.then((resultFromController) => res.send(resultFromController));
});


// Route for Retrieving All Orders

router.get("/", auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
	};
	orderController
		.getAllOrders(data)
		.then((resultFromController) => res.send(resultFromController))
});

// Route for Retrieving User's Orders

router.get("/myorders", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	const data = {
		userId: userData.id,
		isAdmin: userData.isAdmin,
	};
	orderController.getUserOrders(data).then((result) => res.send(result));
});



module.exports = router;
