const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers");
const auth = require("../auth");


/*
Create Product Sample Workflow
  1. An authenticated admin user sends a POST request containing a JWT in its header to the /products endpoint.
  2. API validates JWT, returns false if validation fails.
  3. If validation successful, API creates product using the contents of the request body.
*/

// Route for Adding a Product

router.post("/", auth.verify, (req, res) => {

	const userData = auth.decode(
		req.headers.authorization)

	if(userData.isAdmin) {

		productController.addProduct(req.body).then(
			resultFromController => res.send(
			resultFromController))

	} else {
		res.send("Only Admins are allowed to add a product.")
	}
});


// Route for Retrieving ALL active Products

router.get("/", (req, res) => {

	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});


// Route for Retrieving a Single Product

router.get("/:productId", (req, res) => {
	console.log(req.params)

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
});


// Route for Updating a Product

router.put("/:productId", auth.verify, (req, res) => {

	const data = {
		productId: req.params.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		updateProduct: req.body
	}

	productController.updateProduct(data).then(resultFromController => res.send(resultFromController))
});


// Route to Archive a Product

router.put("/:productId/archive", auth.verify, (req, res) => {

	const data = {
		productId: req.params.productId,
		payload: auth.decode(req.headers.authorization).isAdmin
	}

	productController.archiveProduct(data).then(resultFromController => res.send(resultFromController))
});




module.exports = router;