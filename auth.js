const jwt = require("jsonwebtoken");
const secret = "EcommerceAPI";

module.exports.createAccessToken = (user) => {

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {});
}


// Token Verification

module.exports.verify = (req, res, next) => {

	let token = req.headers.authorization;

	if(typeof token !== "undefined") {

		token = token.slice(7, token.length);

	return jwt.verify(token, secret, (err, data) => {

		if(err){
			return res.send({auth: "failed"});
		} else {
			next();
		}
	})

	} else {
		return res.send({auth: "failed"});
	}
}


// Token Decryption

module.exports.decode = (token) => {

	if(typeof token !== "undefined"){

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {

			if (err) {
				return null
			} else {

				return jwt.decode(token, {complete: true}).payload;
			}
		})
	} else {

		return null
	}
}


/*
Set User as Admin Sample Workflow
 1. An authenticated admin user sends a PUT request containing a JWT in its header to the /:userId/setAsAdmin endpoint.
 2. API validates JWT, returns false if validation fails.
 3. If validation successful, API finds user with ID matching the userId URL parameter and sets its isAdmin property to true.*/